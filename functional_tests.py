from selenium import webdriver

# Prepare chrome headless to run on Pipeline
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("headless")
chrome_options.add_argument("disable-gpu")
chrome_options.add_argument("no-sandbox")
chrome_options.add_argument("window-size=1920x1080")

browser = webdriver.Chrome(options=chrome_options)
browser.get("http://localhost:8000")

assert "Django" in browser.title
